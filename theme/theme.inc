<?php
/**
 * @file
 * Theme file for Projeckktor
 */

/**
 * Projekktor template preprocess function.
 */
function template_preprocess_projekktor_container(&$vars) {
  $settings = &$vars['settings'];
  
  // Each Projekktor instance gets a unique id
  $projekktor_id = &drupal_static('projekktor_id', 0);
  $vars['id'] = ++$projekktor_id;
  
  // Prepare elements
  $items = $vars['items'];
  $vars['items'] = array();
  foreach ($items as $delta => $item) {
    // Grab items for the playlist source files
    $url = file_create_url($item['uri']);
    if ($item['filemime'] == 'application/octet-stream') {
      $mime = 'video/youtube';
    }
    else $mime = $item['filemime'];    
    $playlist_item_src = array(
      'src' => $url,
      'type' => $mime,
    );
    // Per video config options array
    if (isset($item['filename'])) {
      $title = $item['filename'];
    }
    else $title = NULL;
    $playlist_item_config = array(
      'title' => $title,
    );

    $vars['playlist_items'][$delta] = array(
      $playlist_item_src,
      'config' => $playlist_item_config,
    );
  }
  $playlist = $vars['playlist_items'];

  // Attach Projekktor JavaScript
  projekktor_add_js($projekktor_id, $settings, $playlist);
}

/**
 * Views template preprocess function
 */
function theme_views_view_projekktor($vars) {
  $items = array();

  $view = $vars['view'];
  $file_field_name = $vars['file_field_name'];
  foreach ($vars['rows'] as $row) {
    $lang = $row->_field_data[$view->base_field]['entity']->language;
    // omit rows without file field.
    if (!isset($row->_field_data[$view->base_field]['entity']->{$file_field_name})) {
      continue;
    }
    $item = $row->_field_data[$view->base_field]['entity']->{$file_field_name}[$lang][0];
    $items[] = $item;
  }

  return theme('projekktor_container', array(
    'items' => $items,
    'settings' => $vars['options'],
  ));
}