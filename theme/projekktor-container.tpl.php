<?php
/**
 * @file
 * Projeckktor template file
 *
 * This template file provides only an obect to which the projekktor javascript
 * can be attached. No files are actually loaded into this template.
 */
?>
<video class="projekktor-content projekktor clearfix" id="projekktor-<?php print $id; ?>" controls />
