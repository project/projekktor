(function($) {
/*
 * Attaches a Projekktor element by using the Projekktor Javascript Library
 */
Drupal.behaviors.projekktor = {
  attach: function(context, settings) {
    $('video, audio', context).once(function() {
      $(this).each(function() {
        var $this = $(this);
        var id = $this.attr('id');
        var jsoptions = settings.projekktor.instances[id];
        var jspath = settings.projekktor.jspath;
        // @WTF: returns 'projekktor is not defined' if $.getScript isn't used
        $.getScript(jspath, function() {
          var player = projekktor('#' + id, jsoptions);
        });
      });
    })
  }
};

})(jQuery);